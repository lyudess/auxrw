#!/usr/bin/python3
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
from itertools import *
from sys import stderr, exit
import errno

def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)

def hexdump(start_address, data):
    output = ('%02x' % b for b in data)
    ascii_output = (c if c.isprintable() else '.'
                    for c in (chr(b) for b in data))

    # If the starting address isn't aligned to 16, add filler so our hexdump
    # stays aligned
    start_pad = start_address % 16
    if start_pad:
        output = chain(repeat('XX', start_pad), output)
        ascii_output = chain(repeat('.', start_pad), ascii_output)
        start_address -= start_pad

    address = count(start_address, 16)

    print("        0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f  0123456789abcdef")
    for column, ascii_column in zip(grouper(output, 16, 'XX'),
                                    grouper(ascii_output, 16, '.')):
        print('%05X: %s  %s' %
              (next(address), ' '.join(column), ''.join(ascii_column)))

class DpcdReadError(Exception):
    pass
class DpcdWriteError(Exception):
    pass

def dpcd_read(aux_dev, address, length):
    try:
        aux_dev.seek(address)
        return aux_dev.read(length)
    except OSError as e:
        raise DpcdReadError() from e

def dpcd_write(aux_dev, address, data):
    try:
        aux_dev.seek(address)
        return aux_dev.write(data)
    except OSError as e:
        raise DpcdWriteError() from e

def format_address_range(address, count):
    if count > 1:
        return "0x%x-0x%x" % (address, (address + count) - 1)
    else:
        return "0x%x" % address

def cmd_read(args):
    if isinstance(args.address, tuple):
        # argument is an address range
        if args.count is not None:
            args.subparser.error(("can't specify both an address range and "
                                  "count"))

        address = args.address[0]
        count = args.address[1] - args.address[0] + 1
    else:
        address = args.address
        count = 1 if args.count is None else args.count

    try:
        hexdump(address, dpcd_read(args.aux_dev, address, count))
    except DpcdReadError as e:
        os_error = e.__cause__

        print("Failed to read %s on %s: %s" %
              (format_address_range(address, count), args.aux_dev.name,
               os_error),
              file=stderr)
        exit(os_error.errno)

def cmd_write(args):
    if isinstance(args.address, tuple):
        # argument is an address range
        address, end = args.address
        data = bytes(islice(cycle(args.bits), end - address + 1))
    else:
        address = args.address
        data = bytes(args.bits)

    try:
        if args.repeat:
            print('Hit ^C to end...')
            while True:
                dpcd_write(args.aux_dev, address, data)
        else:
            dpcd_write(args.aux_dev, address, data)
            if args.read_back:
                hexdump(address, dpcd_read(args.aux_dev, address, len(data)))
    except (DpcdReadError, DpcdWriteError) as e:
        os_error = e.__cause__

        if isinstance(e, DpcdReadError):
            prefix = "read back"
        else:
            prefix = "write"

        print("Failed to %s %s on %s: %s" %
              (prefix, format_address_range(address, len(data)),
               args.aux_dev.name, os_error),
              file=stderr)
        exit(os_error.errno)


def uint(arg):
    try:
        uint = int(arg)
    except TypeError:
        raise argparse.ArgumentTypeError('not a valid int' % arg)

    if uint < 0:
        raise argparse.ArgumentTypeError('cannot be less than 0')

    return uint

def uhex(arg):
    try:
        if arg.startswith('0x'):
            arg = arg[2:]
        uhex = int(arg, base=16)
    except TypeError:
        raise argparse.ArgumentTypeError('not a valid hex number' % arg)

    if uhex < 0:
        raise argparse.ArgumentTypeError('cannot be less than 0')

    return uhex

def bit(arg):
    bit = uhex(arg)
    if bit > 0xFF:
        raise argparse.ArgumentTypeError('must be between 0x0 and 0xFF')

    return bit

def address_range(arg):
    if '-' in arg:
        start, end = (uhex(ui) for ui in arg.split('-', 2))
        if start > end:
            raise argparse.ArgumentTypeError('invalid address range')

        return start, end
    else:
        return uhex(arg)

class ArgumentParser(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):
        kwargs['allow_abbrev'] = True
        kwargs['formatter_class'] = argparse.ArgumentDefaultsHelpFormatter

        super().__init__(*args, **kwargs)

        self.set_defaults(subparser=self)

    def add_subparsers(self, *args, **kwargs):
        kwargs['parser_class'] = self.__class__
        return super().add_subparsers(*args, **kwargs)


try:
    parser = ArgumentParser(
        description="Linux DPCD register tool. Use at your own risk!"
    )
    subparsers = parser.add_subparsers(help='commands')

    parser_read = subparsers.add_parser('read', aliases='r',
                                        help='Read a range of DPCD registers')
    parser_read.add_argument('aux_dev', metavar='aux_device',
                             help=('The DRM DP AUX device to read from (e.g.'
                                   ' /dev/drm_dp_aux0, /dev/drm_dp_aux1,'
                                   ' etc.)'),
                             type=argparse.FileType('rb+', 0))
    parser_read.add_argument('address',
                             help=('The starting DPCD address to read from,'
                                   ' or an address range of registers to read'
                                   ' (specified as <start>-<end>)'),
                             type=address_range)
    parser_read.add_argument('count', help='How many bytes to read',
                             type=uint, nargs='?')
    parser_read.set_defaults(func=cmd_read)

    parser_write = subparsers.add_parser('write', aliases='w',
                                         help=('Write to a range of DPCD'
                                               ' registers'))
    parser_write.add_argument('aux_dev', metavar='aux_device',
                              help=('The DRM DP AUX device to write to'
                                    ' (e.g. /dev/drm_dp_aux0,'
                                    ' /dev/drm_dp_aux1, etc.)'),
                              type=argparse.FileType('rb+', 0))
    parser_write.add_argument('address',
                              help=('The starting DPCD address to write to,'
                                    ' or an address range of registers to'
                                    ' fill with the given pattern (specified'
                                    ' as <start>-<end>)'),
                              type=address_range)
    parser_write.add_argument('bits', metavar='bit',
                              help=('The bit(s) of data to write to the'
                                    ' register range'),
                              type=bit, nargs='+')
    parser_write_modes = parser_write.add_mutually_exclusive_group()
    parser_write_modes.add_argument('-l', '--repeat',
                                    help=("Continously write the given bits"
                                          " until we're interrupted by ^C"),
                                    default=False, action='store_true')
    parser_write_modes.add_argument('-r', '--read-back',
                                    help=("Read back the contents of the DPCD"
                                          " register after writing to it"),
                                    default=False, action='store_true')
    parser_write.set_defaults(func=cmd_write)

    args = parser.parse_args()
    if 'func' not in args:
        parser.error('No action specified')

    args.func(args)
except KeyboardInterrupt as e:
    exit(errno.EINTR)
