# What is this?

A simple script for writing/reading from DPCD registers on a Linux system directly from userspace. Intended only for developers, use at your own risk!

This is licensed under the GPL v3, see the license file provided for more information.
